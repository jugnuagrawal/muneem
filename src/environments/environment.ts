// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: {
    login: 'http://127.0.0.1:4000/login',
    register: 'http://127.0.0.1:4000/register',
    user: 'http://127.0.0.1:4000/user',
    organization: 'http://127.0.0.1:4001/org',
    inventory: 'http://127.0.0.1:4002/inventory'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
