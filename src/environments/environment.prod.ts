export const environment = {
  production: true,
  url: {
    login: 'http://127.0.0.1:4000/login',
    register: 'http://127.0.0.1:4000/register',
    user: 'http://127.0.0.1:4000/user',
    organization: 'http://127.0.0.1:4001/org',
    inventory: 'http://127.0.0.1:4002/inventory'
  }
};
