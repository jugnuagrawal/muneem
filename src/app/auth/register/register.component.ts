import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { Action } from '../../utils/definition/action';

@Component({
  selector: 'ja-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  action: Action;
  constructor(private commonService: CommonService, private fb: FormBuilder) {
    this.form = this.fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      contactNo: [null, [Validators.required]],
    });
    this.action = {};
  }

  ngOnInit() {
  }

  register() {
    if (this.form.invalid) {
      return;
    }
    this.action.pending = true;
    this.action.message = null;
    this.commonService.post('register', '', this.form.value).subscribe((_res: any) => {
      this.action.pending = false;
      this.action.success = true;
      this.action.message = _res.message;
    }, _err => {
      this.action.pending = false;
      this.action.success = false;
      this.action.message = _err.error.message;
    });
  }

}
