import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { Router } from '@angular/router';
import { Action } from '../../utils/definition/action';
import { UserDetails } from '../../utils/definition/userDetails';

@Component({
  selector: 'ja-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  action: Action;
  constructor(
    private commonService: CommonService,
    private fb: FormBuilder,
    private router: Router) {
    this.form = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
    this.action = {};
  }

  ngOnInit() {
  }

  login() {
    if (this.form.invalid) {
      return;
    }
    this.action.pending = true;
    this.action.message = null;
    this.commonService.post('login', '', this.form.value).subscribe((_res: UserDetails) => {
      this.action.pending = false;
      this.action.success = true;
      this.commonService.token = _res.token;
      this.commonService.userDetails = _res;
      this.router.navigate(['/setup']);
    }, _err => {
      this.action.pending = false;
      this.action.success = false;
      this.action.message = _err.error.message;
    });
  }

  googleDetails(details) {
    console.log(details);
  }
}
