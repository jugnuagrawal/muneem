export interface RequestOptions {
    select?: string;
    sort?: string;
    filter?: any;
    page?: number;
    count?: number;
}