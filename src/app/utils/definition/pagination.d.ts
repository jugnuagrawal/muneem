import { RequestOptions } from "./requestOptions";

export interface Pagination {
    currentPage?: number;
    totalPages?: number;
    totalDocuments?: number;

    init(requestOptions: RequestOptions, totalDocuments: number): void;
    firstPage(): boolean;
    lastPage(): boolean;
}
