export interface UserDetails {
    _id?: string;
    name?: string;
    email?: string;
    contact?: string;
    token?: string;
}