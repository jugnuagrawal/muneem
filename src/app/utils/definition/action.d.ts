export interface Action {
    pending?: boolean;
    success?: boolean;
    message?: string;
}