import { Directive, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';

declare let gapi: any;

@Directive({
  selector: '[jaGoogleAuth]'
})
export class GoogleAuthDirective implements OnInit {

  auth2: any;
  @Output() details: EventEmitter<any>;
  constructor(private element: ElementRef) {
    this.details = new EventEmitter();
  }

  googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '969753944936-jithjrgsu13d2040ii7lgjljedqlpcou.apps.googleusercontent.com',
        scope: 'email profile'
      });
      this.attachSignin(this.element.nativeElement);
    });
  }
  attachSignin(element) {
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        const profile = googleUser.getBasicProfile();
        const userDetails: any = {};
        userDetails.name = profile.getName();
        userDetails.email = profile.getEmail();
        userDetails.token = googleUser.getAuthResponse().id_token;
        this.details.emit(userDetails);
      }, (error) => {
        console.log(error);
        alert(JSON.stringify(error, undefined, 2));
      });
  }

  ngOnInit() {
    this.googleInit();
  }
}
