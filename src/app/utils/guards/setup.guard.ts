import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonService } from '../../services/common.service';
import { Organization } from '../definition/organization';

@Injectable({
  providedIn: 'root'
})
export class SetupGuard implements CanActivate {
  constructor(private commonService: CommonService,
    private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.commonService.get('organization', '', {
      filter: {
        users: this.commonService.userDetails._id
      }
    }).toPromise().then((data: Array<Organization>) => {
      this.commonService.organizationList = data;
      if (data && data.length > 0) {
        this.router.navigate(['/organization']);
        return false;
      } else {
        return true;
      }
    }).catch(err => {
      return true;
    });
  }
}
