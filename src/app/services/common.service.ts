import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { RequestOptions } from '../utils/definition/requestOptions';
import { UserDetails } from '../utils/definition/userDetails';
import { Organization } from '../utils/definition/organization';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  token: string;
  userDetails: UserDetails;
  organizationList: Array<Organization>;
  selectedOrganization: Organization;
  constructor(private http: HttpClient,
    private router: Router) {
    this.token = localStorage.getItem('m-token');
    this.userDetails = {};
    this.organizationList = [];
    this.selectedOrganization = {};
  }

  get(key, path, options?: RequestOptions) {
    let url = environment.url[key] + path;
    return this.http.get(url, {
      headers: this.getHeaders()
    });
  }

  post(key, path, payload, options?: RequestOptions) {
    let url = environment.url[key] + path;
    return this.http.post(url, payload, {
      headers: this.getHeaders()
    });
  }

  put(key, path, payload, options?: RequestOptions) {
    let url = environment.url[key] + path;
    return this.http.put(url, payload, {
      headers: this.getHeaders()
    });
  }

  delete(key, path, options?: RequestOptions) {
    let url = environment.url[key] + path;
    return this.http.delete(url, {
      headers: this.getHeaders()
    });
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['auth']);
  }

  private getHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('authorization', this.token);
    return headers;
  }
}
