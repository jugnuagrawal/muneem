import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { AuthComponent } from './auth/auth.component';
import { SetupComponent } from './setup/setup.component';
import { SetupGuard } from './utils/guards/setup.guard';
import { OrganizationComponent } from './organization/organization.component';
import { OrganizationGuard } from './utils/guards/organization.guard';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'auth' },
    { path: 'welcome', component: WelcomeComponent },
    { path: 'auth', component: AuthComponent },
    { path: 'setup', component: SetupComponent, canActivate: [SetupGuard] },
    { path: 'organization', component: OrganizationComponent, canActivate: [OrganizationGuard] },
    { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            useHash: true
        })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouterModule { }
