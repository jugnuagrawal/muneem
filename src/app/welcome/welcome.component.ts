import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'ja-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  form:FormGroup;
  constructor(private commonService:CommonService,private fb:FormBuilder) {
    this.form = this.fb.group({

    });
  }

  ngOnInit() {
  }

}
