import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Action } from '../utils/definition/action';
import { CommonService } from '../services/common.service';
import { Organization } from '../utils/definition/organization';

@Component({
  selector: 'ja-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {

  userType: number;
  form: FormGroup;
  action: Action;
  constructor(
    private commonService: CommonService,
    private fb: FormBuilder,
    private router: Router) {
    this.userType = 0;
    this.form = this.fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      type: ['', [Validators.required]],
      contactNos: this.fb.array([
        this.fb.group({
          code: [],
          number: []
        })
      ]),
      address: [null],
      users: []
    });
    this.action = {};
  }

  ngOnInit() {
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.action.pending = true;
    this.action.message = null;
    this.form.get('users').patchValue([this.commonService.userDetails._id]);
    if (this.commonService.userDetails.contact) {
      this.form.get(['contactNos', 0, 'code']).patchValue('+91');
      this.form.get(['contactNos', 0, 'code']).patchValue(this.commonService.userDetails.contact);
    }
    this.commonService.post('organization', '', this.form.value).subscribe((_res: Organization) => {
      this.commonService.selectedOrganization = _res;
      this.commonService.organizationList.push(_res);
      this.action.pending = false;
      this.action.success = true;
      this.router.navigate(['./dashboard']);
    }, _err => {
      this.action.pending = false;
      this.action.success = false;
      this.action.message = _err.error.message;
    });
  }
}
