import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Inventory } from '../../utils/definition/inventory';
import { RequestOptions } from '../../utils/definition/requestOptions';
import { Action } from '../../utils/definition/action';
import { Pagination } from '../../utils/definition/pagination';

export class InventoryPagination implements Pagination {
  currentPage: number;
  totalPages: number;
  totalDocuments: number;
  requestOptions: RequestOptions;
  constructor() {
    this.currentPage = 0;
    this.totalPages = 1;
    this.totalDocuments = 30;
  }

  init(requestOptions: RequestOptions, totalDocuments: number) {
    this.requestOptions = requestOptions;
    this.totalDocuments = totalDocuments;
    this.totalPages = Math.ceil(totalDocuments / requestOptions.count);
  }

  firstPage() {
    if (this.currentPage == 0) {
      return true;
    }
    return false;
  }

  lastPage() {
    if (this.currentPage == this.totalPages) {
      return true;
    }
    return false;
  }
}

@Component({
  selector: 'ja-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  inventoryList: Array<Inventory>;
  pagination: InventoryPagination;
  action: Action;
  requestOptions: RequestOptions;
  constructor(private commonService: CommonService) {
    this.inventoryList = [];
    this.action = {};
    this.requestOptions = {
      count: 30,
      page: 1
    };
    this.pagination = new InventoryPagination();
  }

  ngOnInit() {
    this.getCount();
  }

  getCount() {
    this.commonService.get('inventory', '/count', { filter: this.requestOptions.filter }).subscribe((count: number) => {
      this.pagination.init(this.requestOptions, count);
      this.loadData();
    }, err => {

    });
  }

  loadData() {
    this.commonService.get('inventory', '', this.requestOptions).subscribe((data: Array<Inventory>) => {
      this.inventoryList = data;
    }, err => {

    });
  }

  nextPage() {
    if (this.pagination.lastPage()) {
      return;
    }
    this.requestOptions.page += 1;
    this.pagination.currentPage += 1;
    this.loadData();
  }

  prevPage() {
    if (this.pagination.firstPage()) {
      return;
    }
    this.requestOptions.page -= 1;
    this.pagination.currentPage -= 1;
    this.loadData();
  }
}
