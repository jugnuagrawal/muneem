import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'ja-inventory-manage',
  templateUrl: './inventory-manage.component.html',
  styleUrls: ['./inventory-manage.component.scss']
})
export class InventoryManageComponent implements OnInit {

  form: FormGroup;
  constructor(private commonService: CommonService,
    private fb: FormBuilder) {
    this.form = this.fb.group({
      name: [null, [Validators.required]],
      itemId: [null],
      brand: [null],
      quantity: [null],
      price: [null],
      manufactureDate: [null],
      description: [null],
      barcode: [null],
      organization: [null],
      addedBy: [null],
      partner: [null]
    });
  }

  ngOnInit() {
  }

}
