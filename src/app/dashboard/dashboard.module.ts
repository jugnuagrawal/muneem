import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { InventoryComponent } from './inventory/inventory.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { UserManageComponent } from './users/user-manage/user-manage.component';
import { InventoryManageComponent } from './inventory/inventory-manage/inventory-manage.component';
import { InvoiceManageComponent } from './invoice/invoice-manage/invoice-manage.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      { path: 'home', component: HomeComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'settings', component: SettingsComponent },
      {
        path: 'users', component: UsersComponent, children: [
          { path: ':id', component: UserManageComponent }
        ]
      },
      {
        path: 'inventory', component: InventoryComponent, children: [
          { path: ':id', component: InventoryManageComponent }
        ]
      },
      {
        path: 'invoice', component: InvoiceComponent, children: [
          { path: ':id', component: InvoiceManageComponent }
        ]
      }
    ]
  },
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    DashboardComponent,
    ProfileComponent,
    HomeComponent,
    SettingsComponent,
    InventoryComponent,
    InventoryManageComponent,
    UsersComponent,
    UserManageComponent,
    InvoiceComponent,
    InvoiceManageComponent
  ]
})
export class DashboardModule { }
