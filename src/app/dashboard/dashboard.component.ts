import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { trigger, transition, style, group, query, animate } from '@angular/animations';
import { CommonService } from '../services/common.service';
import { Organization } from '../utils/definition/organization';

export const routerTransition = trigger('routerTransition', [
  transition('* <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({ position: 'fixed', width: '100%', 'z-index': -1 })
      , { optional: true }),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.25s ease-in-out', style({ transform: 'translateX(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateX(0%)' }),
        animate('0.25s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ], { optional: true }),
    ])
  ])
])
@Component({
  selector: 'ja-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [routerTransition]
})
export class DashboardComponent implements OnInit {

  toggleSideNav: boolean;
  form: FormGroup;
  selectedOrganization: Organization;
  constructor(private commonService: CommonService, private fb: FormBuilder) {
    this.form = this.fb.group({

    });
    this.selectedOrganization = {};
  }

  ngOnInit() {
    this.selectedOrganization = this.commonService.selectedOrganization;
  }

  logout() {
    this.commonService.logout();
  }

  getState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
