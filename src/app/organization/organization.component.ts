import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Organization } from '../utils/definition/organization';
import { CommonService } from '../services/common.service';
import { Action } from '../utils/definition/action';

@Component({
  selector: 'ja-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {

  @ViewChild('addOrganizationModal') addOrganizationModal: TemplateRef<HTMLElement>;
  organizationList: Array<Organization>;
  form: FormGroup;
  action: Action;
  addOrganizationModalRef: NgbModalRef;
  constructor(private commonService: CommonService,
    private router: Router,
    private fb: FormBuilder,
    private modalService: NgbModal) {
    this.form = this.fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      type: ['', [Validators.required]],
      contactNos: this.fb.array([
        this.fb.group({
          code: [],
          number: []
        })
      ]),
      address: [null],
      users: []
    });
    this.action = {};
  }

  ngOnInit() {
    this.organizationList = this.commonService.organizationList;
  }

  selectOrganization(org: Organization) {
    this.commonService.selectedOrganization = org;
    this.router.navigate(['/dashboard']);
  }

  addOrganization() {
    this.addOrganizationModalRef = this.modalService.open(this.addOrganizationModal, { centered: true });
    this.addOrganizationModalRef.result.then(_reason => {
      this.form.reset();
    }).catch(_err => {
      this.form.reset();
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.action.pending = true;
    this.action.message = null;
    this.form.get('users').patchValue([this.commonService.userDetails._id]);
    if (this.commonService.userDetails.contact) {
      this.form.get(['contactNos', 0, 'code']).patchValue('+91');
      this.form.get(['contactNos', 0, 'code']).patchValue(this.commonService.userDetails.contact);
    }
    this.commonService.post('organization', '', this.form.value).subscribe((_res: Organization) => {
      this.commonService.selectedOrganization = _res;
      this.commonService.organizationList.push(_res);
      this.action.pending = false;
      this.action.success = true;
      this.addOrganizationModalRef.close();
      this.router.navigate(['./dashboard']);
    }, _err => {
      this.action.pending = false;
      this.action.success = false;
      this.action.message = _err.error.message;
    });
  }
}
